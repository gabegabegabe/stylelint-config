# stylelint-config
Gabe's stylelint config

## Example Config
For a SASS/SCSS project, your config should look like this:
```js
module.exports = {
	extends: ['@gabegabegabe/stylelint-config/scss']
};
```
