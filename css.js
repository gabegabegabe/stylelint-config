export default {
	/* eslint-disable sort-keys */
	extends: 'stylelint-config-standard',
	rules: {
		// Avoid Errors
		// // Unknown
		'declaration-property-value-no-unknown': true,
		'media-feature-name-value-no-unknown': true,
		'no-unknown-animations': true,
		'no-unknown-custom-media': true,
		'no-unknown-custom-properties': true,

		// Enforce Conventions
		// // Allowed, Disallowed, and Required
		// // // Color
		'color-named': 'never',

		// // // Declaration
		'unit-disallowed-list': [
			'cm',
			'in',
			'mm',
			'pc',
			'pt',
			'px',
			'Q'
		],

		// // Case
		'value-keyword-case': ['lower', { camelCaseSvgKeywords: true }],

		// // Empty Lines
		'comment-empty-line-before': [
			'always',
			{
				except: ['first-nested'],
				ignore: ['after-comment', 'stylelint-commands']
			}
		],
		'rule-empty-line-before': [
			'always',
			{
				except: [
					'after-single-line-comment',
					'first-nested'
				]
			}
		],

		// // // Media Feature
		'media-feature-name-unit-allowed-list': {
			'/height/': ['em', 'rem'],
			'/width/': ['em', 'rem']
		},

		// // Notation
		'font-weight-notation': 'numeric',
		'selector-pseudo-element-colon-notation': 'single',

		// // Quotes
		'font-family-name-quotes': 'always-where-recommended',
	}
	/* eslint-enable sort-keys */
};
