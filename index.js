export default {
	// This contains an empty rules objects simply to prevent stylelint from
	// complaining about there being no rules in the base config.  The other
	// configs provide actual rules and should be used in overrides arrays
	// targeting the appropriate filetypes.
	rules: {}
};
