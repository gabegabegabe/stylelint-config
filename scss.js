import cssConfig from './css.js';

const { rules: cssRules } = cssConfig;

const NAMED_ARGUMENTS_OPTION = [
	'always',
	{ ignore: ['single-argument'] }
];

// Must begin with an alpha character
// Must be at least two characters long
// May contain alphanumeric characters
// Alpha characters must be lowercase
// Words may be separated by dashes
// Must end with an alphanumeric character
const ITEM_NAME_REGEX = /^[a-z](?:[a-z0-9]*-)*[a-z0-9]+$/u;

const ITEM_NAME_REGEX_MESSAGE = `
	Must begin with an alpha character
	Must be at least two characters long
	May contain alphanumeric characters
	Alpha characters must be lowercase
	Words may be separated by dashes
	Must end with an alphanumeric character
`.trim();

export default {
	extends: ['stylelint-config-standard-scss'],
	rules: {
		/* eslint-disable sort-keys */
		// Bring in the base CSS rules again, to make sure our configuration isn't
		// overriden by stylelint-config-standard-scss.
		...cssRules,

		// Disable a base rule
		'declaration-property-value-no-unknown': null,

		// SCSS rules
		'scss/at-each-key-value-single-line': true,
		'scss/at-extend-no-missing-placeholder': true,
		'scss/at-function-named-arguments': NAMED_ARGUMENTS_OPTION,
		'scss/at-function-parentheses-space-before': 'always',
		'scss/at-function-pattern': [
			ITEM_NAME_REGEX,
			{
				message: ITEM_NAME_REGEX_MESSAGE
			}
		],
		'scss/at-if-no-null': true,
		'scss/at-mixin-named-arguments': NAMED_ARGUMENTS_OPTION,
		'scss/at-mixin-parentheses-space-before': 'always',
		'scss/at-mixin-pattern': [
			ITEM_NAME_REGEX,
			{
				message: ITEM_NAME_REGEX_MESSAGE
			}
		],
		'scss/at-rule-no-unknown': true,
		'scss/dollar-variable-colon-newline-after': 'always-multi-line',
		'scss/dollar-variable-empty-line-after': [
			'always',
			{ except: ['last-nested', 'before-dollar-variable'] }
		],
		'scss/dollar-variable-empty-line-before': [
			'always',
			{ except: ['first-nested', 'after-comment', 'after-dollar-variable'] }
		],
		'scss/dollar-variable-first-in-block': [
			true,
			{ ignore: ['comments', 'imports'] }
		],
		'scss/dollar-variable-no-missing-interpolation': true,
		'scss/dollar-variable-pattern': [
			ITEM_NAME_REGEX,
			{
				message: ITEM_NAME_REGEX_MESSAGE
			}
		],
		'scss/percent-placeholder-pattern': [
			ITEM_NAME_REGEX,
			{
				message: ITEM_NAME_REGEX_MESSAGE
			}
		],
		'scss/comment-no-empty': true,
		'scss/comment-no-loud': true,
		'scss/declaration-nested-properties': 'never',
		'scss/dimension-no-non-numeric-values': true,
		'scss/function-quote-no-quoted-strings-inside': true,
		'scss/function-unquote-no-unquoted-strings-inside': true,
		'scss/load-no-partial-leading-underscore': true,
		'scss/map-keys-quotes': 'always',
		'scss/media-feature-value-dollar-variable': 'always',
		'scss/operator-no-newline-after': true,
		'scss/operator-no-newline-before': true,
		'scss/operator-no-unspaced': true,
		'scss/partial-no-import': true,
		'scss/selector-no-redundant-nesting-selector': true,
		'scss/selector-no-union-class-name': true,
		'scss/no-duplicate-dollar-variables': true,
		'scss/no-duplicate-mixins': true,
		'scss/no-global-function-names': true
		/* eslint-enable sort-keys */
	}
};
